const fs = require('fs')
fs.readFile(`${__dirname}/curses.json`, (err, data)=>{
    if(err)throw new Error(err);curses = JSON.parse(`${data}`)
    let listnerCode=``
    for(curse in curses)if(curse!="close")listnerCode+=`:*:${curse}::\r\nRun "audio.vbs" "${curse}"\r\nreturn\r\n`
    fs.writeFileSync(`${__dirname}/listener.ahk`, listnerCode+`#x::\r\nRun "audio.vbs" "close"\r\nExitApp`)
})